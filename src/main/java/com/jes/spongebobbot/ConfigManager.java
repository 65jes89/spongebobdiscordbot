package com.jes.spongebobbot;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import org.json.JSONObject;

public class ConfigManager
{
    private static ConfigManager i = new ConfigManager();
    private final File configFile = new File("config.json");

    private JSONObject jsonConfig;

    public static ConfigManager get()
    {
        return i;
    }

    private ConfigManager()
    {
        System.out.println("Generating config file...");

        try
        {
            if (!this.configFile.exists())
            {
                this.configFile.createNewFile();

                FileWriter out = new FileWriter(this.configFile);

                this.jsonConfig = new JSONObject();
                this.jsonConfig.put("bot_token", "");
                this.jsonConfig.put("command", "!sb");

                out.write(this.jsonConfig.toString());
                out.flush();
            }
            else
            {
                BufferedReader br = new BufferedReader(new FileReader(this.configFile));
                StringBuilder jsonString = new StringBuilder();
                String line = br.readLine();
                while(line != null)
                {
                    jsonString.append(line);
                    line = br.readLine();
                }

                this.jsonConfig = new JSONObject(jsonString.toString());
            }
        }
        catch (IOException e)
        {
            System.out.println("Issue loading config file.");
            e.printStackTrace();
            System.exit(-1);
        }
    }

    public String getBotToken()
    {
        return this.jsonConfig.getString("bot_token");
    }

    public String getCommand()
    {
        return this.jsonConfig.getString("command");
    }
}