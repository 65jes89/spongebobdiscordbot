package com.jes.spongebobbot;

import javax.security.auth.login.LoginException;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;

public class BotManager
{
    private static BotManager i = new BotManager();
    private JDA jda;

    public static BotManager get()
    {
        return i;
    }

    public JDA getJDA()
    {
        return this.jda;
    }

    public void init(String token) throws LoginException, InterruptedException
    {
        this.jda = new JDABuilder(token)
                .addEventListener(new EngineSpongebob())
                .build();
        this.jda.awaitReady();
    }
}