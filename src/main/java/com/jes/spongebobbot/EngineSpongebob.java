package com.jes.spongebobbot;

import java.util.Random;

import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.MessageHistory;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

public class EngineSpongebob extends ListenerAdapter
{
    public void onMessageReceived(MessageReceivedEvent event)
    {
        Message receivedMessage = event.getMessage();
        if (receivedMessage.getContentDisplay().equals(ConfigManager.get().getCommand()))
        {
            if (!receivedMessage.getAuthor().isBot())
            {
                receivedMessage.delete().queue();

                MessageChannel channel = event.getChannel();
                MessageHistory history = channel.getHistoryBefore(receivedMessage, 1).complete();

                Message messageToSpongebob = history.getRetrievedHistory().get(0);
                String msg = messageToSpongebob.getContentDisplay();
                messageToSpongebob.delete().queue();

                String outputMessage = this.spongebob(msg);
                channel.sendMessage(outputMessage).queue();
            }
        }
    }

    private String spongebob(String msg)
    {
        char[] chars = msg.toCharArray();
        char[] ret = new char[chars.length];

        for(int i = 0; i < chars.length; ++i)
        {
            char j = chars[i];
            if (Character.isLetter(j))
            {
                boolean shouldSpongebob = (new Random()).nextBoolean();
                if (shouldSpongebob)
                {
                    j = this.swapCase(j);
                }
            }
            ret[i] = j;
        }
        return new String(ret);
    }

    private char swapCase(char i)
    {
        if (Character.isUpperCase(i))
        {
            i = Character.toLowerCase(i);
        }
        else if (Character.isLowerCase(i))
        {
            i = Character.toUpperCase(i);
        }
        return i;
    }
}