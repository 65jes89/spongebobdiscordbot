package com.jes.spongebobbot;

import javax.security.auth.login.LoginException;

public class Main
{
    public static void main(String[] args) throws LoginException, InterruptedException
    {
        BotManager botManager = new BotManager();
        String token = ConfigManager.get().getBotToken();
        botManager.init(token);
    }
}
